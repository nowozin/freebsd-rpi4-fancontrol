
# FreeBSD Raspberry Pi 4 temperature-adaptive fan control

The Raspberry Pi 4 case I bought came with a fan that can be controlled using
GPIO pin 14.  The case is listed on Amazon.co.uk as [GeeekPi Raspberry Pi 4
Aluminum NAS Case](https://www.amazon.co.uk/dp/B0BRSQV68H).

The code in this folder contains a simple adaptive temperature control program,
reading the CPU temperature from the `dev.cpu.0.temperature` kernel value and
choosing the fan intensity adaptively.  The fan is controlled using pulse width
modulation (PWM).

## Usage

To build the program, run `make all`.  You can test if the fan is properly
operated by running the `shutdown_fan` and `start_fan` programs.  If the fan
stops and starts once you execute this program (using `sudo ./shutdown_fan` and
`sudo ./start_fan`), you know it works.

The `fancontrol` program is run in daemon mode and for consistent control of
the fan speed, there should only be a single process operating the fan.

## Automatic fan control after every reboot

To start the `fancontrol` program automatically after every reboot, copy the
`fancontrol` binary to `/usr/local/sbin`.  Then edit root's crontab using the
`sudo crontab -u root -e` command, then adding the line:

```
@reboot /usr/local/sbin/fancontrol
```

This will start the `fancontrol` program after every reboot.

