#include <sys/types.h>
#include <sys/sysctl.h>
#include <assert.h>
#include <err.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>
#include <libgpio.h>

// Measure temperature no more frequent than this many seconds
#define TEMPERATURE_MEASUREMENT_INTERVAL 5
#define SOFT_PWM_FREQUENCY 100

/* Main control policy
 *
 * Arguments:
 *   temperature_celsius: measured CPU temperature in Celsius.
 *
 * Returns intended fan intensity between 0.0 and 1.0.
 */
double policy(double temperature_celsius) {
	if (temperature_celsius <= 55.0)
		return 0.0;

	if (temperature_celsius <= 65.0)
		return 0.7;

	if (temperature_celsius <= 75.0)
		return 0.85;

	return 1.0;
}

#define CPU_DATA_BUFLEN	8
double get_cpu_temperature(void) {
	size_t data_len = CPU_DATA_BUFLEN;
	int data[CPU_DATA_BUFLEN];
	int result_code = sysctlbyname(
		"dev.cpu.0.temperature", data, &data_len, NULL, 0);

	if (result_code == -1)
		return -1.0;	// Failed to obtain temperature

	// Convert temperature into Celsius
	double temperature = data[0];
	temperature /= 10.0;
	temperature -= 273.15;

	return temperature;
}

enum PWM_state {
	LOW,
	HIGH,
};

/* Calculate the PWM cycle times for LOW and HIGH states.
 *
 * Arguments:
 *   frequency: number of cycles per second, for example 100.
 *   intensity: >= 0.0, <= 1.0.
 *   state: which time to compute (LOW or HIGH).
 *
 * Return the single-cycle LOW or HIGH time in microseconds.
 */
useconds_t get_pwm_duration(int frequency, double intensity, enum PWM_state state) {
	assert(intensity >= 0.0 && intensity <= 1.0);
	assert(state == LOW || state == HIGH);

	double cycle_time_us = 1000000.0 / ((double) frequency);
	double high_time_us = intensity * cycle_time_us;
	double low_time_us = (1.0 - intensity) * cycle_time_us;
	if (state == LOW) {
		return ((useconds_t) low_time_us);
	} else if (state == HIGH) {
		return ((useconds_t) high_time_us);
	}
	return 0;
}

enum temperature_control_state {
	FAN_OFF,
	FAN_FULL,
	FAN_PWM_LOW,
	FAN_PWM_HIGH,
};

enum temperature_control_state control_state;

void fan_off(gpio_handle_t handle) {
	gpio_pin_low(handle, 14);
}
void fan_on(gpio_handle_t handle) {
	gpio_pin_high(handle, 14);
}

int main(int argc, char* argv[]) {
	// Run as daemon
	pid_t pid = fork();
	if (pid < 0) {
		exit(EXIT_FAILURE);
	}
	if (pid > 0) {
		// Parent, exit
		exit(EXIT_SUCCESS);
	}

	// Child: infinite temperature control
	gpio_handle_t handle = gpio_open(0);
	if (handle == GPIO_INVALID_HANDLE) {
		err(1, "gpio_open failed");
		exit(1);
	}

	gpio_pin_output(handle, 14);

	// Switch fan off
	fan_off(handle);
	control_state = FAN_OFF;

	time_t last_temperature_measurement = 0;
	double temperature = -1.0;
	double fan_intensity = 0.0;
	useconds_t fan_low_time_us = 0;
	useconds_t fan_high_time_us = 0;

	// Main control loop
	for (unsigned int loop_index = 0; true; ++loop_index) {
		if (loop_index % 10 == 0) {	// approximately once a second
			time_t current = time(NULL);
			if ((current - last_temperature_measurement) >= TEMPERATURE_MEASUREMENT_INTERVAL) {
				temperature = get_cpu_temperature();
				last_temperature_measurement = current;
				printf("Temperature: %.1f Celsius\n", temperature);

				// Temperature control policy
				fan_intensity = policy(temperature);
				if (fan_intensity <= 0.0) {
					control_state = FAN_OFF;
					printf("  => fan to OFF\n");
					fan_off(handle);
				} else if (fan_intensity >= 1.0) {
					control_state = FAN_FULL;
					printf("  => fan to FULL\n");
					fan_on(handle);
				} else {
					printf("  => intensity to %.2f%%\n", 100.0*fan_intensity);
					control_state = FAN_PWM_LOW;
					fan_low_time_us = get_pwm_duration(SOFT_PWM_FREQUENCY, fan_intensity, LOW);
					fan_high_time_us = get_pwm_duration(SOFT_PWM_FREQUENCY, fan_intensity, HIGH);
				}
			}
		}

		switch (control_state) {
			case FAN_OFF:
			case FAN_FULL:
				usleep(50000);
				break;
			case FAN_PWM_LOW:
				// Is LOW, set to HIGH
				fan_on(handle);
				usleep(fan_high_time_us);
				control_state = FAN_PWM_HIGH;
				break;
			case FAN_PWM_HIGH:
				// Is HIGH, set to LOW
				fan_off(handle);
				usleep(fan_low_time_us);
				control_state = FAN_PWM_LOW;
				break;
			default:
				assert(0);
				break;
		}
	}

	gpio_close(handle);
}

