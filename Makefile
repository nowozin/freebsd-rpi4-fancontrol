
CFLAGS=-O -Wall

all:	fancontrol start_fan shutdown_fan

clean:
	rm -f fancontrol start_fan shutdown_fan

fancontrol:	fancontrol.c
	$(CC) $(CFLAGS) -o fancontrol fancontrol.c -lgpio

shutdown_fan:	shutdown_fan.c
	$(CC) $(CFLAGS) -o shutdown_fan shutdown_fan.c -lgpio

start_fan:	start_fan.c
	$(CC) $(CFLAGS) -o start_fan start_fan.c -lgpio
