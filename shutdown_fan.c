#include <sys/types.h>
#include <err.h>
#include <stdlib.h>
#include <libgpio.h>

int main(int argc, char* argv[]) {
	gpio_handle_t handle = gpio_open(0);
	if (handle == GPIO_INVALID_HANDLE) {
		err(1, "gpio_open failed");
		exit(1);
	}

	gpio_pin_output(handle, 14);
	gpio_pin_low(handle, 14);
	gpio_close(handle);
}

